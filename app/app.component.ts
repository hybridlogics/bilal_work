import {Component} from 'angular2/core'; // <-- importing Component from core
import {ROUTER_PROVIDERS, ROUTER_DIRECTIVES, RouteConfig} from 'angular2/router';

@Component({
    selector: 'my-app',
  templateUrl: 'app/app.html',
  styleUrls: ['app/app.css']
})


export class AppComponent {
	title: string;
  artists: any;
  constructor() {
    this.title = '';
    this.artists = [
      {
        name: 'Rodney Erickson',
        id: 1,
        bio: 'Rodney Erickson is a content marketing professional at HubSpot, an inbound marketing and sales platform that helps companies attract visitors, convert leads, and close customers. Previously, Rodney worked as a marketing manager for a tech software startup. He graduated with honors from Columbia University with a dual degree in Business Administration and Creative Writing.'
      },
      { 
        name: 'Alex Honeysett',
        id: 2,
        bio: 'Alex Honeysett is a Brand and Marketing Strategist who partners with CEOs, executives and solopreneurs to grow their personal and professional brands, human-to-human. After spending nearly a decade working in PR and marketing for multimillion dollar brands and startups, Alex knows what truly drives conversions, sold-out launches, and *New York Times* interviews—and it’s not mastering the marketing flavor of the week. It’s how well you connect with the heart-beating people you’re trying to help and communicate your understanding back to them. Alex has landed coverage in print and broadcast outlets around the world, including the Today Show, *Wall Street Journal*, Mashable, BBC, NPR, and CNN. Her own articles have been featured in The Muse, *Forbes*, *Inc.*, Mashable, DailyWorth, and *Newsweek*. In addition to her extensive PR and marketing experience, Alex is a trained business coach.'
      },
      { 
        name: 'Cassie Boorn',
        id: 3,
        bio: 'Cassie is the Visionary behind Maker Mentors as well as the Founder of Modern Thrive, an online platform designed to help you figure out what you want to do with your life. For the past 5 years she has been a writer and entrepreneur, managing online marketing programs for some of the worlds largest brands. Cassie is passionate about connecting the right people together to help them make amazing things happen.'
      }]
  }
  
  onClick(myName, myElement) {
    this.title=myName;
    myElement.style.backgroundColor='#FECE4E';
  }
  
  addArtist(myArtistName, myArtistBio) {
  console.log(myArtistName);
  console.log(myArtistBio);
    this.artists.push({'name': myArtistName, 'bio': myArtistBio});
    console.log(this.artists);
  }	
} // <--- we need to export the class AppComponent.  